// set up ======================================================================
var express = require('express');
var session = require('express-session');
var cookieParser = require('cookie-parser'); // the session is stored in a cookie, so we use this to parse it
var app = express();            // create our app w/ express
var mongoose = require('mongoose');         // mongoose for mongodb
var port = process.env.PORT || 3000;        // set the port
var database = require('./config/database');      // load the database config
var morgan = require('morgan');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
// var multer  = require('multer');
// var storage =   multer.diskStorage({
//   destination: function (req, file, callback) {
//     callback(null, './upload');
//   },
//   filename: function (req, file, callback) {
//     callback(null, file.originalname);
//   }
// });
// var upload = multer({ storage : storage}).single('userPhoto');
// var upload = multer({ dest: './upload/' });

// ---------------------------------------------------
// var multiparty = require('connect-multiparty');

// var multipartyMiddleware = multiparty();
// app.use(multiparty({
//     uploadDir: '/upload'
// }));

// ---------------------------------------------------

var sessionOptions = {
  secret: "secret",
  resave : true,
  saveUninitialized : true,
  cookie: { maxAge: 86400000 }
};

app.use(session(sessionOptions));

// configuration ===============================================================
mongoose.connect(database.localUrl);  // Connect to local MongoDB instance. A remoteUrl is also available (modulus.io)

app.use(express.static(__dirname )); //public những đường dẫn để có load vào lấy thư viện hay hình ảnh nói chung nodejs không cho phép nếu không public o đây

app.use(express.static(__dirname + '/public'));     // set the static files location /public/img will be /img for users
app.use(express.static(__dirname + '/bower_components'));     // set the static files location /public/img will be /img for users
app.use(express.static(__dirname + '/upload/img'));     // set the static files location /public/img will be /img for users
app.use(morgan('dev')); // log every request to the console
app.use(bodyParser.urlencoded({'extended': 'true'})); // parse application/x-www-form-urlencoded
app.use(bodyParser.json()); // parse application/json
app.use(bodyParser.json({type: 'application/vnd.api+json'})); // parse application/vnd.api+json as json
app.use(methodOverride('X-HTTP-Method-Override')); // override with the X-HTTP-Method-Override header in the request

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "http://localhost");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

// routes ======================================================================
require('./app/routes.js')(app);

// listen (start app with node server.js) ======================================
app.listen(port);
console.log("App listening on port " + port);

