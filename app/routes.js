var mongo = require('mongodb');
var User =  require('./controllers/user');
var NhomTin =  require('./controllers/nhomtin');//
var TinTuc =  require('./controllers/tintuc');//
var DocGia =  require('./controllers/docgia');//
var BinhLuan =  require('./controllers/binhluan');//
var QuangCao =  require('./controllers/quangcao');//
var Login =  require('./controllers/auth');


// var multiparty = require('connect-multiparty');
// var multipartyMiddleware = multiparty({
//     uploadDir: '/upload'
// });

// var multer = require('multer');
// var upload = multer({
//   dest: __dirname + './upload/',
// });


module.exports = function (app) {

    // app.all('/api/*', function(req, res,next){// kiểm tra login nè. ** req.originalUrl link request api **
    //     var exceptList = ["/api/login","/api/isLogin"];// danh sách những api cho phép truy cập khi chư login
    //     if(exceptList.indexOf(req.originalUrl) != -1 || req.session.user){ // kiểm tra nếu mà chưa đăng nhập thì sẽ không có session
    //     // if(req.session.user){ // kiểm tra nếu mà chưa đăng nhập thì sẽ không có session
    //         next();
    //     }else{
    //         res.send({"data":false,"authentication":false});
    //     }
    // });

    //user
    app.get('/api/users',User.getUsers);
    app.post('/api/users', User.createUser);
    app.delete('/api/users/:user_id', User.deleteUser);
    app.get('/api/users/:user_id',User.getUser);
    app.put('/api/users/:user_id',User.updateUser);

    //Nhom Tin
    app.get('/api/nhom-tin',NhomTin.getNhomTin);
    app.get('/api/nhom-tin/:id',NhomTin.getNhomTinById);
    app.post('/api/nhom-tin', NhomTin.createNhomTin);
    app.delete('/api/nhom-tin/:id', NhomTin.deleteNhomTin);
    app.put('/api/nhom-tin/:id', NhomTin.update);
    app.get('/api/nhom-tin-page/:itemPage', NhomTin.getNhomTinPaginate);//--------------------------------------------------------

    //Tin Tuc
    app.get('/api/tin-tuc',TinTuc.getTinTuc);
    app.get('/api/tin-tuc/sort/:sort',TinTuc.getTinTucSort);
    app.get('/api/tin-tuc/:id',TinTuc.getTinTucById);
    app.get('/api/tin-tuc-loai/:IDNhom',TinTuc.getTinTucByCat);
    app.post('/api/tin-tuc', TinTuc.createTinTuc);
    app.post('/api/tin-tuc-data', TinTuc.createTinTucData);
    // app.post('/api/tin-tuc', multipartyMiddleware, TinTuc.createTinTuc);
    // app.post('/api/tin-tuc', upload.single('upl'), TinTuc.createTinTuc);
    // app.post('/api/tin-tuc', upload.single('avatar'), TinTuc.createTinTuc);
    app.delete('/api/tin-tuc/:id', TinTuc.deleteTinTuc);
    app.get('/api/the-loai-tin/:id',TinTuc.getTinTucbyNhomTin);
    app.put('/api/tin-tuc/:id', TinTuc.update);
    // app.post('/api/tin-tuc/:id', multipartyMiddleware, TinTuc.update);

    //Binh Luan
    app.get('/api/binh-luan',BinhLuan.getBinhLuan);
    app.get('/api/binh-luan/:id',BinhLuan.getOne);
    app.get('/api/binh-luan-tin/:id',BinhLuan.getByNew);
    app.post('/api/binh-luan', BinhLuan.create);
    app.delete('/api/binh-luan/:id', BinhLuan.delete);
    app.put('/api/binh-luan/:id', BinhLuan.update);

    //Quang Cao
    app.get('/api/quang-cao',QuangCao.get);
    app.get('/api/quang-cao/:id',QuangCao.getOne);
    app.post('/api/quang-cao', QuangCao.create);
    app.delete('/api/quang-cao/:id', QuangCao.delete);
    app.put('/api/quang-cao/:id', QuangCao.update);


    //Doc Gia
    app.get('/api/doc-gia',DocGia.getDocGia);
    app.post('/api/doc-gia', DocGia.createDocGia);
    app.delete('/api/doc-gia/:id', DocGia.deleteDocGia);

    //login
    app.post('/api/login', Login.login);
    app.get('/api/isLogin', Login.isLogin);
    app.get('/api/logout', Login.logout);


    // application -------------------------------------------------------------
    app.get('', function (req, res) {
        res.sendFile('./public/index.html'); // load the single view file (angular will handle the page changes on the front-end)
    });

};

