'use strict';

var mongoose = require('mongoose'),
    DocGia = require('../models/docgia.js'),
    mongo = require('mongodb');

// module.exports = function (app) {

// }

function getDocGia(res) {
    DocGia.find(function (err, docgia) {

        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            res.send(err);
        }
        res.json({"data":docgia}); // return all users in JSON format
    });
}

exports.getDocGia = function (req, res) {
    getDocGia(res);
};

exports.createDocGia = function (req, res) {
    // create a user, information comes from AJAX request from Angular
    DocGia.create({
        TaiKhoan: req.body.TaiKhoan,
        MatKhau: req.body.MatKhau,
        TenTaiKhoan: req.body.TenTaiKhoan
    }, function (err, docgia) {
        if (err)
            res.send(err);

        // get and return all the users after you create another
        getDocGia(res);
    });
};

exports.deleteDocGia = function (req, res) {
    DocGia.remove({
        _id: req.body._id
    }, function (err, docgia) {
        if (err)
            res.send(err);

        getDocGia(res);
    });
};