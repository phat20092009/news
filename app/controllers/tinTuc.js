'use strict';

var mongoose = require('mongoose'),
    TinTuc = require('../models/tintuc.js'),
    mongo = require('mongodb');
var _ = require('underscore');

//----------------------------------------------------------
var multer = require('multer');
var storage = multer.diskStorage({ //multers disk storage settings
    destination: function (req, file, cb) {
        cb(null, './upload/img')
    },
    filename: function (req, file, cb) {
        // var datetimestamp = Date.now();
        // cb(null, file.fieldname + '-' + datetimestamp + '.' + file.originalname.split('.')[file.originalname.split('.').length -1])
        cb(null, file.originalname);
        // console.log('file----------',file);
        // console.log('req.body----------',req.body);
        // console.log('req.body----------',req.body);

    }
});
var upload = multer({ //multer settings
                storage: storage
            }).single('file');
//----------------------------------------------------------

function getTinTuc(req,res) {
    TinTuc.find().populate('NhomTin').exec(function(err, tintuc) {
        if (err) {
            res.send(err);
        }
        res.json({"data":tintuc}); // return all users in JSON format
    });
}

exports.getTinTucbyNhomTin = function(req,res) {
    var nhomTin = req.params.id || "";
    console.log('nhomTin',nhomTin);
    // TinTuc.find({"NhomTin":nhomTin}, null, {sort: '-NgayDang'}, function (err, tintuc) {
    TinTuc.find({"NhomTin":nhomTin, "KichHoat": true}, null, {sort: '-NgayDang'}, function (err, tintuc) {

        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            res.send(err);
        }
        console.log('tintuc',tintuc);
        res.json({"data":tintuc}); // return all users in JSON format
    });
}

exports.getTinTucSort = function(req,res) {
    var sort = req.params.sort || "";
    TinTuc.find({ "KichHoat": true}, null, {sort: sort}, function (err, tintuc) {

        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            res.send(err);
        }
        res.json({"data":tintuc}); // return all users in JSON format
    });
}

exports.getTinTucByCat = function(req,res) {
    var IDNhom = req.params.IDNhom || "";
    // TinTuc.find({'NhomTin':new mongo.ObjectID(IDNhom)}, function(err, tintuc) {
    //     // if there is an error retrieving, send the error. nothing after res.send(err) will execute
    //     if (err) {
    //         res.send(err);
    //     }
    //     res.json({"data":tintuc}); // return all users in JSON format
    // });
    
    TinTuc.find({'NhomTin':new mongo.ObjectID(IDNhom),"KichHoat": true}).populate('NhomTin').exec(function(err, tintuc) {
        if (err) {
            res.send(err);
        }
        res.json({"data":tintuc}); // return all users in JSON format
    });
}

exports.getTinTuc = function (req, res) {
    getTinTuc(req,res);
};

exports.getTinTucById = function(req, res) {
    var id = new mongo.ObjectID(req.params.id);
    TinTuc.findOne({'_id':id}, function(err, tintuc) {
        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            res.send(err);
        }
        res.json({"data":tintuc}); // return all users in JSON format
    });
};

exports.createTinTuc = function(req, res,next) {
    upload(req,res,function(err){
        if(err){
             res.json({error_code:1,err_desc:err});
             return;
        }
        // console.log('res----------',res);
        res.json({error_code:0,err_desc:null});
    })
    // var file = req.files.file;
    // console.log('req.files----------',req.files);
    // console.log('file.name',file.name);
    // console.log('req.body-----------------------------------',req.body );
    // res.json({"data":"tessssss"}); // return all users in JSON format
    // res.json({"data":"tessssss"}); // return all users in JSON format

    // TinTuc.create({
    //     TieuDe: req.body.TieuDe,
    //     // Anh: file.name,
    //     NoiDung: req.body.NoiDung,
    //     KichHoat: req.body.KichHoat,
    //     NgayDang: req.body.NgayDang || Date.now(),
    //     NhomTin: new mongo.ObjectID(req.body.NhomTin)
    // }, function (err, tintuc) {
    //     if (err)
    //         res.send(err);

    //     // get and return all the users after you create another
    //     // getTinTuc(req,res);
    //     res.json({"data":"Tạo bài viết thành công"}); // return all users in JSON format
    // });
};
exports.createTinTucData = function(req, res,next) {
    
    TinTuc.create({
        TieuDe:     req.body.TieuDe,
        Anh:        req.body.Anh,
        NoiDung:    req.body.NoiDung,
        KichHoat:   req.body.KichHoat,
        NgayDang:   req.body.NgayDang || Date.now(),
        NhomTin:    new mongo.ObjectID(req.body.NhomTin)
    }, function (err, tintuc) {
        if (err)
            res.send(err);

        // get and return all the users after you create another
        // getTinTuc(req,res);
        res.json({"data":"Tạo bài viết thành công"}); // return all users in JSON format
    });
};

exports.deleteTinTuc = function (req, res) {
    TinTuc.remove({
        _id: req.params.id
    }, function (err, tintuc) {
        if (err)
            res.send(err);

        res.json({"data":"Xóa tin thành công"}); // return all users in JSON format
        // getTinTuc(req,res);
    });
};

exports.update = function (req, res) {
    TinTuc.findById(req.params.id, function(err, tintuc) {

        if (err)
            res.send(err);
        if (req.body.TieuDe)
            tintuc.TieuDe = req.body.TieuDe;
        if (req.body.NoiDung)
            tintuc.NoiDung = req.body.NoiDung;
        // if (file.name)
        //     tintuc.Anh = file.name;  // update the bears info
        if (req.body.NhomTin)
            tintuc.NhomTin = req.body.NhomTin; 
        if (req.body.Anh)
            tintuc.Anh = req.body.Anh; 

        tintuc.KichHoat = req.body.KichHoat;

        // save the bear
        tintuc.save(function(err,resultNhomtin) {
            if (err)
                res.send(err);

            // res.json({"data":resultUser}); // return all users in JSON format
            res.json({"data":"Cập nhật tin tức thành công"}); // return all users in JSON format
            res.end();
        });

    });
}