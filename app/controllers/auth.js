'use strict';

var mongoose = require('mongoose'),
    User = mongoose.model('User'),
    mongo = require('mongodb');

exports.login = function (req, res) {
    User.findOne({
        username: req.body.username,
        password: req.body.password,
    },function (err, user) {

        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            res.send(err);
        }
        // if(user && user.length){
        if(user){
            // console.log('user---',user.quyen);
            req.session.user = user;
            res.json({"data":user}); // return all users in JSON format
        }else{
            res.json({"data":false});
        }
    });
};

exports.isLogin =  function (req, res) {
    if(req.session.user){
        res.json({"data":true});
    }else{
        res.json({"data":false});
    }
};
exports.logout =  function (req, res) {
    req.session.destroy(function(err) {
      if(err) {
        res.json({"data":false});
      } else {
        res.json({"data":true});
      }
    });
};



