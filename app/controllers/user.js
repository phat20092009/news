'use strict';

var mongoose = require('mongoose'),
    User = require('../models/user.js'),
    mongo = require('mongodb');

// module.exports = function (app) {

// }

function getUsers(res) {
    User.find(function (err, users) {

        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            res.send(err);
        }
        res.json({"data":users}); // return all users in JSON format
    });
}

exports.getUsers = function (req, res) {
    getUsers(res);
};

exports.createUser = function (req, res) {
    // create a user, information comes from AJAX request from Angular
    User.create({
        username: req.body.username,
        password: req.body.password,
        TenTaiKhoan: req.body.TenTaiKhoan,
        quyen: req.body.quyen
    }, function (err, user) {
        if (err)
            res.send(err);

        // get and return all the users after you create another
        // getUsers(res);
        res.json({"data":"Tạo tài khoản thành công"}); // return all users in JSON format
    });
};

exports.deleteUser = function (req, res) {
    User.remove({
        _id: req.params.user_id
    }, function (err, user) {
        if (err)
            res.send(err);

        // getUsers(res);
        res.json({"data":"Xóa tài khoản thành công"}); // return all users in JSON format
    });
};

exports.getUser = function (req, res) {
    User.findById(req.params.user_id,function (err, user) {
        if (err)
            res.send(err);  
        res.json({"data":user}); // return all users in JSON format
    });
}

exports.updateUser = function (req, res) {
    User.findById(req.params.user_id, function(err, user) {

        if (err)
            res.send(err);
        if (req.body.password)
            user.password = req.body.password;  // update the bears info
        if (req.body.TenTaiKhoan)
            user.TenTaiKhoan = req.body.TenTaiKhoan;  // update the bears info
        if (req.body.quyen)
            user.quyen = req.body.quyen;  // update the bears info

        // save the bear
        user.save(function(err,resultUser) {
            if (err)
                res.send(err);

            // res.json({"data":resultUser}); // return all users in JSON format
            res.json({"data":"Cập nhật tài khoản thành công"}); // return all users in JSON format
        });

    });
}