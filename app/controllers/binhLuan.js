'use strict';

var mongoose = require('mongoose'),
    BinhLuan = require('../models/binhluan.js'),
    mongo = require('mongodb');

// module.exports = function (app) {

// }

function getBinhLuan(res) {
    // BinhLuan.find(function (err, binhluan) {
    //     if (err) {
    //         res.send(err);
    //     }
    //     res.json({"data":binhluan}); // return all users in JSON format
    // });

    BinhLuan.find().populate('DocGia').populate('TinTuc').exec(function(err, binhluan) {
        if (err)
            res.send(err);
        res.json({"data":binhluan}); // return all owners in JSON format
    });

}

exports.getBinhLuan = function (req, res) {
    getBinhLuan(res);
};

// exports.createBinhLuan = function (req, res) {
//     // create a user, information comes from AJAX request from Angular
//     BinhLuan.create({
//         TieuDe: req.body.TieuDe,
//         NoiDung: req.body.NoiDung,
//         NgayDang: req.body.NgayDang,
//         MaDocGia: new mongo.ObjectID(req.body.MaDocGia),
//         MaTinTuc: new mongo.ObjectID(req.body.MaTinTuc)
//     }, function (err, binhluan) {
//         if (err)
//             res.send(err);

//         // get and return all the users after you create another
//         getBinhLuan(res);
//     });
// };

// exports.deleteBinhLuan = function (req, res) {
//     BinhLuan.remove({
//         _id: req.body.NhomTin
//     }, function (err, nhomtin) {
//         if (err)
//             res.send(err);

//         getTinTuc(res);
//     });
// };

exports.getOne = function(req, res) {
    var id = new mongo.ObjectID(req.params.id);
    BinhLuan.findOne({'_id':id}, function(err, binhluan) {
        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            res.send(err);
        }
        res.json({"data":binhluan}); // return all users in JSON format
    });
};

exports.getByNew = function(req, res) {
    var id = new mongo.ObjectID(req.params.id);
    // BinhLuan.find({'TinTuc':id}, function(err, binhluan) {
    BinhLuan.find({'TinTuc':id}).populate('TinTuc').populate('DocGia').exec(function(err, binhluan) {
        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            res.send(err);
        }
        res.json({"data":binhluan}); // return all users in JSON format
    });

};

exports.create = function (req, res) {
    var input = req.body.data;
    console.log('input',input);
    input.DocGia = new mongo.ObjectID(input.DocGia);
    input.TinTuc = new mongo.ObjectID(input.TinTuc);
    BinhLuan.collection.insert(input,function(err,binhluan){
        if (err)
            res.send(err);       
        res.json({"data":binhluan,"alert":"Thêm bình luận thành công"});
    });    
};

exports.delete = function (req, res) {
    BinhLuan.remove({
        _id: req.params.id
    }, function (err, binhluan) {
        if (err)
            res.send(err);

        // getUsers(res);
        res.json({"data":"Xóa bình luận thành công"}); // return all users in JSON format
    });
};

exports.update = function (req, res) {
    BinhLuan.findById(req.params.id, function(err, binhluan) {

        if (err)
            res.send(err);
        if (req.body.TieuDe)
            binhluan.TieuDe = req.body.TieuDe;  // update the bears info
        if (req.body.NoiDung)
            binhluan.NoiDung = req.body.NoiDung;  // update the bears info
        if (req.body.MaDocGia)
            binhluan.MaDocGia = req.body.MaDocGia;  // update the bears info
        if (req.body.MaTinTuc)
            binhluan.MaTinTuc = req.body.MaTinTuc;  // update the bears info

        // save the bear
        binhluan.save(function(err,resultNhomtin) {
            if (err)
                res.send(err);

            // res.json({"data":resultUser}); // return all users in JSON format
            res.json({"data":"Cập nhật bình luận thành công"}); // return all users in JSON format
        });

    });
}