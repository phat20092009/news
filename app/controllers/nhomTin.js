'use strict';

var mongoose = require('mongoose'),
    NhomTin = require('../models/nhomtin.js'),
    mongo = require('mongodb');
// app.use(paginate.middleware(10, 50));
function getNhomTin(res) {
    NhomTin.find(function (err, nhomtin) {

        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            res.send(err);
        }
        res.json({"data":nhomtin}); // return all users in JSON format
    });
}
// app.get('/users', function(req, res, next) {
exports.getNhomTinPaginate = function (req, res, next) {
    var itemPage;
    var itemPageOne;
    if(req.params.itemPage && req.params.itemPage > 0 ){
        var itemPageOne = req.params.itemPage - 1;
        itemPage = 5 * itemPageOne;
    }else{
        itemPage = 0;
    }
    NhomTin.find().skip(itemPage).limit(5).exec(function(err, nhomtin) {
        if (err) {
            res.send(err);
        }
        res.json({"data":nhomtin}); // return all users in JSON format
    });

}
// });


exports.getNhomTin = function (req, res) {
    getNhomTin(res);
};

exports.getNhomTinById = function(req, res) {
    var id = new mongo.ObjectID(req.params.id);
    NhomTin.findOne({'_id':id}, function(err, nhomtin) {
        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            res.send(err);
        }
        res.json({"data":nhomtin}); // return all users in JSON format
    });
};

exports.createNhomTin = function (req, res) {
    var input = req.body.data;

    NhomTin.collection.insert(input,function(err,nhomtin){
        if (err)
            res.send(err);       
        res.json({"data":nhomtin,"alert":"Thêm nhóm tin thành công"});
    });    
};

exports.deleteNhomTin = function (req, res) {
    NhomTin.remove({
        _id: req.params.id
    }, function (err, nhomtin) {
        if (err)
            res.send(err);

        // getUsers(res);
        res.json({"data":"Xóa nhóm tin thành công"}); // return all users in JSON format
    });
};

exports.update = function (req, res) {
    NhomTin.findById(req.params.id, function(err, nhomtin) {
        console.log('req.body',req.body);
        console.log('nhomtin',nhomtin);
        if (err)
            res.send(err);
        if (req.body.TenNhom)
            nhomtin.TenNhom = req.body.TenNhom;  // update the bears info
        if (req.body.ThuTu)
            nhomtin.ThuTu = req.body.ThuTu;  // update the bears info
        if (req.body.KichHoat)
            nhomtin.KichHoat = req.body.KichHoat;  // update the bears info

        // save the bear
        nhomtin.save(function(err,resultNhomtin) {
            if (err)
                res.send(err);

            // res.json({"data":resultUser}); // return all users in JSON format
            res.json({"data":"Cập nhật tài khoản thành công"}); // return all users in JSON format
        });

    });
}