'use strict';

var mongoose = require('mongoose'),
    QuangCao = require('../models/quangcao.js'),
    mongo = require('mongodb');

// module.exports = function (app) {

// }

function getQuangCao(res) {
    // QuangCao.find(function (err, quangcao) {
    //     if (err) {
    //         res.send(err);
    //     }
    //     res.json({"data":quangcao}); // return all users in JSON format
    // });

    QuangCao.find().populate('DocGia').populate('TinTuc').exec(function(err, quangcao) {
        if (err)
            res.send(err);
        res.json({"data":quangcao}); // return all owners in JSON format
    });

}

exports.get = function (req, res) {
    getQuangCao(res);
};

// exports.createQuangCao = function (req, res) {
//     // create a user, information comes from AJAX request from Angular
//     QuangCao.create({
//         TieuDe: req.body.TieuDe,
//         NoiDung: req.body.NoiDung,
//         NgayDang: req.body.NgayDang,
//         MaDocGia: new mongo.ObjectID(req.body.MaDocGia),
//         MaTinTuc: new mongo.ObjectID(req.body.MaTinTuc)
//     }, function (err, quangcao) {
//         if (err)
//             res.send(err);

//         // get and return all the users after you create another
//         getQuangCao(res);
//     });
// };

// exports.deleteQuangCao = function (req, res) {
//     QuangCao.remove({
//         _id: req.body.NhomTin
//     }, function (err, nhomtin) {
//         if (err)
//             res.send(err);

//         getTinTuc(res);
//     });
// };

exports.getOne = function(req, res) {
    var id = new mongo.ObjectID(req.params.id);
    QuangCao.findOne({'_id':id}, function(err, quangcao) {
        // if there is an error retrieving, send the error. nothing after res.send(err) will execute
        if (err) {
            res.send(err);
        }
        res.json({"data":quangcao}); // return all users in JSON format
    });
};

exports.create = function (req, res) {
    var input = req.body.data;
    QuangCao.collection.insert(input,function(err,quangcao){
        if (err)
            res.send(err);       
        res.json({"data":quangcao,"alert":"Thêm quảng cáo thành công"});
    });    
};

exports.delete = function (req, res) {
    QuangCao.remove({
        _id: req.params.id
    }, function (err, quangcao) {
        if (err)
            res.send(err);

        // getUsers(res);
        res.json({"data":"Xóa quảng cáo thành công"}); // return all users in JSON format
    });
};

exports.update = function (req, res) {
    QuangCao.findById(req.params.id, function(err, quangcao) {

        if (err)
            res.send(err);
        if (req.body.TenQuangCao)
            quangcao.TenQuangCao = req.body.TenQuangCao;  // update the bears info
        if (req.body.Anh)
            quangcao.Anh = req.body.Anh;  // update the bears info
        if (req.body.TrangQuangCao)
            quangcao.TrangQuangCao = req.body.TrangQuangCao;  // update the bears info
        if (req.body.ViTri)
            quangcao.ViTri = req.body.ViTri;  // update the bears info
        if (req.body.ThuTu)
            quangcao.ThuTu = req.body.ThuTu;  // update the bears info

        // save the bear
        quangcao.save(function(err,resultNhomtin) {
            if (err)
                res.send(err);

            // res.json({"data":resultUser}); // return all users in JSON format
            res.json({"data":"Cập nhật quảng cáo thành công"}); // return all users in JSON format
        });

    });
}