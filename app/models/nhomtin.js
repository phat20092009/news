var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var nhomTinSchema = new Schema({
    TenNhom: { type: String },
    ThuTu: { type: Number },
    KichHoat: { type: Boolean }
});

module.exports = mongoose.model('NhomTin', nhomTinSchema);