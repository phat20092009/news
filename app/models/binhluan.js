var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var binhLuanSchema = new Schema({
    TieuDe: { type: String },
    NoiDung: { type: String },
    NgayDang: { type: Date },
    DocGia: {type: Schema.Types.ObjectId, ref: 'User'},
    TinTuc: {type: Schema.Types.ObjectId, ref: 'TinTuc'}
});

module.exports = mongoose.model('BinhLuan', binhLuanSchema);