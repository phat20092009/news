var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var tinTucSchema = new Schema({
    TieuDe: { type: String },
    Anh: { type: String },
    NoiDung: { type: String },
    NgayDang: { type: Date},
    KichHoat: { type: Boolean },
    NhomTin: {type: Schema.Types.ObjectId, ref: 'NhomTin' }
});

module.exports = mongoose.model('TinTuc', tinTucSchema);