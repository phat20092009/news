var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var quangCaoSchema = new Schema({
    TenQuangCao: { type: String },
    Anh: { type: String },
    TrangQuangCao: { type: String },
    ViTri: { type: String },
    ThuTu: { type: Number }
});

module.exports = mongoose.model('QuangCao', quangCaoSchema);