var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var docGiaSchema = new Schema({
    TaiKhoan: { type: String },
    MatKhau: { type: String },
    TenTaiKhoan: { type: String }
});

module.exports = mongoose.model('DocGia', docGiaSchema);