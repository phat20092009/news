var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var UserSchema = new Schema({
    username: { type: String },
    password: { type: String },
    TenTaiKhoan: { type: String },
    quyen: { type: Number }
});

module.exports = mongoose.model('User', UserSchema);