'use strict';
var newsApp = angular.module('calcuApp', [
	'ngRoute',
	'userCtrl',
	'authCtrl',
	'tinTucCtrl',
	'nhomTinCtrl',
	'binhLuanCtrl',
	'docGiaCtrl',
	'quangCaoCtrl',
	'homeCtrl',
	'userService',
	'customService',
	'nhomTinService',
	'tinTucService',
	'binhLuanService',
	'docGiaService',
	'quangCaoService',
	'ngFileUpload',
	'mgcrea.ngStrap',
	'angularUtils.directives.dirPagination',
	'ngStorage'
]);
// Thiết lập ứng dụng
newsApp.config(['$routeProvider', function($routeProvider) {
	$routeProvider.
	when('/admin', {
		// templateUrl: 'js/views/admin/master.html',
		// controller: 'AuthLoginCtrl'
	}).
	when('/admin/login', {
		templateUrl: 'js/views/admin/master.html',
		controller: 'AuthLoginCtrl',
		controllerAs: 'vm',
		activetab: 'login'
	}).
	when('/admin/user', {
		templateUrl: 'js/views/admin/master.html',
		controller: 'UserListCtrl',
		controllerAs: 'vm',
		activetab: 'user'
	}).
	when('/admin/create-user', {
		templateUrl: 'js/views/admin/master.html',
		controller: 'CreateUserListCtrl',
		controllerAs: 'vm',
		activetab: 'createUser'
	}).
	when('/admin/update-user/:user_id', {
		templateUrl: 'js/views/admin/master.html',
		controller: 'UpdateUserListCtrl',
		controllerAs: 'vm',
		activetab: 'updateUser'
	}).
	when('/admin/nhom-tin', {
		templateUrl: 'js/views/admin/master.html',
		controller: 'NhomTinDanhSachCtrl',
		controllerAs: 'vm',
		activetab: 'nhomtin'
	}).
	when('/admin/tao-nhom-tin', {
		templateUrl: 'js/views/admin/master.html',
		controller: 'NhomTinTaoCtrl',
		controllerAs: 'vm',
		activetab: 'taotintuc'
	}).
	when('/admin/sua-nhom-tin/:id', {
		templateUrl: 'js/views/admin/master.html',
		controller: 'NhomTinSuaCtrl',
		controllerAs: 'vm',
		activetab: 'suatintuc'
	}).
	when('/admin/tin-tuc', {
		templateUrl: 'js/views/admin/master.html',
		controller: 'TinTucDanhSachCtrl',
		controllerAs: 'vm',
		activetab: 'tintuc'
	}).
	when('/admin/tao-tin-tuc', {
		templateUrl: 'js/views/admin/master.html',
		controller: 'TaoTinTucCtrl',
		controllerAs: 'vm',
		activetab: 'taotintuc'
	}).
	when('/admin/cap-nhat-tin-tuc/:id', {
		templateUrl: 'js/views/admin/master.html',
		controller: 'CapNhatTinTucCtrl',
		controllerAs: 'vm',
		activetab: 'capnhattintuc'
	}).
	when('/admin/quang-cao', {
		templateUrl: 'js/views/admin/master.html',
		controller: 'QuangCaoDanhSachCtrl',
		controllerAs: 'vm',
		activetab: 'quangcao'
	}).
	when('/admin/tao-quang-cao', {
		templateUrl: 'js/views/admin/master.html',
		controller: 'QuangCaoTaoCtrl',
		controllerAs: 'vm',
		activetab: 'quangcao'
	}).
	when('/admin/sua-quang-cao/:id', {
		templateUrl: 'js/views/admin/master.html',
		controller: 'QuangCaoSuaCtrl',
		controllerAs: 'vm',
		activetab: 'quangcao'
	}).
	when('/admin/binh-luan', {
		templateUrl: 'js/views/admin/master.html',
		controller: 'BinhLuanDanhSachCtrl',
		controllerAs: 'vm',
		activetab: 'binhluan'
	}).
	when('/admin/doc-gia', {
		templateUrl: 'js/views/admin/master.html',
		controller: 'DocGiaDanhSachCtrl',
		controllerAs: 'vm',
		activetab: 'docgia'
	}).
	when('/trang-chu', {
		templateUrl: 'js/views/home/master.html',
		controller: 'HomeListCtrl',
		controllerAs: 'vm',
		activetab: 'trangchu'
	}).
	when('/tin-tuc/:id', {
		templateUrl: 'js/views/home/master.html',
		controller: 'TinTucCtrl',
		controllerAs: 'vm',
		activetab: 'tintuc'
	}).
	when('/loai-tin/:id', {
		templateUrl: 'js/views/home/master.html',
		controller: 'TheLoaiCtrl',
		controllerAs: 'vm',
		activetab: 'theloai'
	}).
	otherwise({
		// redirectTo: '/admin/user'
		redirectTo: '/trang-chu'
	});
}]);

newsApp.controller("mainController", ['$scope', '$rootScope', '$location', '$route', '$localStorage', 'User', 'NhomTin', 'Custom',mainController]);
function mainController($scope, $rootScope, $location, $route, $localStorage, User, NhomTin, Custom) {
	var mainVM = this;
    // $rootScope.header = false;
    mainVM.$location = $location;
    mainVM.$route = $route;

    if($localStorage.user){
    	$rootScope.rootUserLog = $localStorage.user;
    }
  //   else{
		// $location.path('trang-chu');
  //   }
    if($localStorage.user && $localStorage.user.quyen == 2 ){
		$location.path('trang-chu');
    }
  	// console.log('$rootScope.$storage',$localStorage.user);
  	// $rootScope.userLog = $localStorage.user;
  	// console.log('mainVM.userLog',$rootScope.userLog);
  	// mainVM.TenTaiKhoan = Custom.getUser();
  	// console.log('Custom.getUser',Custom.getUser());
  //   if($rootScope.$storage){
		// $rootScope.$storage = $localStorage.$default({
		// 	user: $rootScope.$storage.user
		// });
  //   }
    mainVM.isPageLogin = function(location){
    	if(location == "/admin/login"){
    		return true;
    	}
    	return false;
    }

    /* logout */
	var logoutAPI = function(){
		delete $localStorage.user;
		User.logout()
			.success(function(result) {
				if(result.data == true){
					$location.path('admin/login');
				}
			}).error(function(err){
		    });
	}

	mainVM.logout = function(){
		logoutAPI();
	}

	var NhomTinMenuTop = function(){
		NhomTin.get()
			.success(function(result) {
				mainVM.nhomTinMenuTop = result.data;
			}).error(function(err){
		    });
	}
	NhomTinMenuTop();
	/* end logout */
}

// Khởi động ứng dụng
newsApp.run(['$rootScope', '$location', '$localStorage', function($rootScope, $location,$localStorage) {
	$rootScope.link = function(link) {
		var hashPrefix = '#/';
		return hashPrefix + link;
	};

	// Kiểm tra Backend hay Frontend
	$rootScope.isBackend = false;
	$rootScope.isBackendFunc = function(url) {
		if (/^\/admin.*$/.test(url) === true) {
			$rootScope.isBackend = true;
		} else {
			$rootScope.isBackend = false;
		}
		return $rootScope.isBackend;
	};

	// Sự kiện $routeChangeStart
	$rootScope.$on('$routeChangeStart', function(event) {
		var currentUrl = $location.path();
		if ($rootScope.isBackendFunc(currentUrl)) {
			// console.log('Backend');
		    // if($localStorage.user){
		    // 	$rootScope.rootUserLog = $localStorage.user;
		    // }
			if($localStorage.user && $localStorage.user.quyen == 2   ){
				$location.path('trang-chu');
			}
		} else {
			// console.log('Frontend');
		}
	});
}]);

