angular.module('quangCaoService', [])
	.factory('QuangCao', ['$http',function($http) {
		return {
			get : function() {
				return $http.get('/api/quang-cao');
			},			
			getOne : function(id) {
				return $http.get('/api/quang-cao/' + id);
			},			
			update : function(data) {
				return $http.put('/api/quang-cao/' + data._id,data);
			},
			create : function(data) {
				return $http.post('/api/quang-cao', data);
			},
			delete : function(id) {
				return $http.delete('/api/quang-cao/' + id);
			}
		}
	}]);