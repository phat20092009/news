angular.module('docGiaService', [])
	.factory('DocGia', ['$http',function($http) {
		return {
			get : function() {
				return $http.get('/api/doc-gia');
			},
			create : function(docGiaData) {
				return $http.post('/api/doc-gia', docGiaData);
			},
			delete : function(id) {
				return $http.delete('/api/doc-gia/' + id);
			}
		}
	}]);