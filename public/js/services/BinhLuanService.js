angular.module('binhLuanService', [])
	.factory('BinhLuan', ['$http',function($http) {
		return {
			get : function() {
				return $http.get('/api/binh-luan');
			},
			getOneNew : function(id) {
				return $http.get('/api/binh-luan-tin/' + id);
			},
			create : function(binhLuanData) {
				return $http.post('/api/binh-luan', binhLuanData);
			},
			delete : function(id) {
				return $http.delete('/api/binh-luan/' + id);
			}
		}
	}]);