'use strict';
angular.module('customService', [])
	.factory('Custom', ['$rootScope', function($rootScope) {
	// Thiết lập đường dẫn tới View
	this.setPath = function(path) {
		$rootScope.path = path;
	};
	this.getPath = function() {
		return $rootScope.path;
	};

	// Thiết lập tiêu đề
	this.setTitle = function(title) {
		$rootScope.pageTitle = title;
	};
	this.getTitle = function() {
		return $rootScope.pageTitle;
	};

	// Thiết lập tai khoan
	this.setUser = function(user) {
		$rootScope.UserLog = user;
	};
	this.getUser = function() {
		return $rootScope.UserLog;
	};

	var flashMessages = '';
	this.setFlashMessages = function(messages) {
		if (messages) {
			flashMessages = messages;
		}
	};

	this.getFlashMessages = function() {
		var messages = flashMessages;
		flashMessages = '';
		return messages ? messages : '';
	};
	
	return this;
}]);