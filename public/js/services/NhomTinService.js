angular.module('nhomTinService', [])
	.factory('NhomTin', ['$http',function($http) {
		return {
			get : function() {
				return $http.get('/api/nhom-tin');
			},			
			getOne : function(id) {
				return $http.get('/api/nhom-tin/' + id);
			},			
			update : function(data) {
				return $http.put('/api/nhom-tin/' + data._id,data);
			},
			create : function(nhomTinData) {
				return $http.post('/api/nhom-tin', nhomTinData);
			},
			delete : function(id) {
				return $http.delete('/api/nhom-tin/' + id);
			}
		}
	}]);