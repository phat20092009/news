angular.module('tinTucService', [])
	.factory('TinTuc', ['$http',function($http) {
		return {
			get : function() {
				return $http.get('/api/tin-tuc');
			},
			getOne : function(id) {
				return $http.get('/api/tin-tuc/' + id);
			},
			getTinTucByCat : function(id) {
				return $http.get('/api/tin-tuc-loai/' + id);
			},
			getCat : function(id) {
				return $http.get('/api/the-loai-tin/' + id);
			},
			getSort : function(sort) {
				return $http.get('/api/tin-tuc/sort/' + sort);
			},
			create : function(tinTucData) {
				return $http.post('/api/tin-tuc', tinTucData);
			},
			createData : function(tinTucData) {
				return $http.post('/api/tin-tuc-data', tinTucData);
			},
			delete : function(id) {
				return $http.delete('/api/tin-tuc/' + id);
			},
			update : function(data) {
				return $http.put('/api/tin-tuc/' + data._id,data);
			}
		}
	}]);