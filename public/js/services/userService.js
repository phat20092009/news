angular.module('userService', [])
	.factory('User', ['$http',function($http) {
		return {
			get : function() {
				return $http.get('/api/users');
			},
			getUser : function(userID) {
				return $http.get('/api/users/' + userID);
			},
			create : function(userData) {
				return $http.post('/api/users', userData);
			},
			update : function(userData) {
				return $http.put('/api/users/' + userData._id,userData);
			},
			delete : function(id) {
				return $http.delete('/api/users/' + id);
			},
			login : function(userData) {
				return $http.post('/api/login',userData);
			},
			logout : function() {
				return $http.get('/api/logout');
			},
			isLogin : function() {
				return $http.get('/api/isLogin');
			}
		}
	}]);