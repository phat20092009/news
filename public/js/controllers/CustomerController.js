'use strict';
var customerCtrl = angular.module('customerCtrl', []);

customerCtrl.controller('CustomerListCtrl', ['$scope', 'Custom', 'Customer', customerController]);
function customerController($scope, Custom, Customer) {
	var vm = this;

	var getAllCustomer = function(){
		Customer.get()
			.success(function(result) {
				vm.customers = result.data;
			}).error(function(err){
		    });
	}
	getAllCustomer();

	Custom.setTitle('Khách hàng');
	Custom.setPath('customers/list');
}