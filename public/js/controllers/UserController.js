'use strict';
var userCtrl = angular.module('userCtrl', []);

userCtrl.controller('UserListCtrl', ['$scope', '$rootScope', '$routeParams', '$location', '$localStorage', 'Custom', 'User', userController]);
function userController($scope, $rootScope, $routeParams, $location, $localStorage, Custom, User) {
	var vm = this;

  	// console.log('Custom.getUser',Custom.getUser());
    if($localStorage.user){
    	$rootScope.rootUserLog = $localStorage.user;
    }
    if($localStorage.user && $localStorage.user.quyen == 2   ){
		$location.path('trang-chu');
    }
	var getAllUser = function(){
		User.get()
			.success(function(result) {
				vm.users = result.data;
			}).error(function(err){
		    });
	}

	var getAllNhomTin = function(){
		console.log('helooooooooooooo');
		NhomTin.get()
			.success(function(result) {
				// vm.users = result.data;
				console.log('result.data',result.data);
			}).error(function(err){
		    });
	}

	vm.goCreateUser = function(){
		$location.path('admin/create-user');
	}

	vm.deleteUser = function(ID){
		User.delete(ID)
			.success(function(result) {
				Custom.setFlashMessages(result.data);
				vm.message = Custom.getFlashMessages();
				getAllUser();
			}).error(function(err){
		    });		
	}

	vm.message = Custom.getFlashMessages();

	getAllUser();

	Custom.setTitle('Người dùng');
	Custom.setPath('users/list');
}

userCtrl.controller('CreateUserListCtrl', ['$scope', '$location', 'Custom', 'User', createUserController]);
function createUserController($scope, $location, Custom, User) {
	var vm = this;
	vm.titlePage = "Tạo người dùng";
	vm.createPage = true;

	vm.createUser = function(){
		// console.log('vm.form',vm.form);
		User.create(vm.form)
			.success(function(result) {
				Custom.setFlashMessages(result.data);
				$location.path('admin/user');
			}).error(function(err){
		    });		
	}

	Custom.setTitle('Tạo người dùng');
	Custom.setPath('users/formUser');
}
userCtrl.controller('UpdateUserListCtrl', ['$scope', '$routeParams', '$location', 'Custom', 'User', updateUserController]);
function updateUserController($scope, $routeParams, $location, Custom, User) {
	var vm = this;
	vm.titlePage = "Cập nhật người dùng";
	vm.editPage = true;

	var getUser = function(){
		User.getUser($routeParams.user_id)
			.success(function(result) {
				vm.form = result.data;
			}).error(function(err){
		    });
	}

	getUser();

	vm.updateUser = function(){
		User.update(vm.form)
			.success(function(result) {
				Custom.setFlashMessages(result.data);
				$location.path('admin/user');
			}).error(function(err){
		    });		
	}

	Custom.setTitle('Tạo người dùng');
	Custom.setPath('users/formUser');
}