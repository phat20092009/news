'use strict';
var binhLuanCtrl = angular.module('binhLuanCtrl', []);

binhLuanCtrl.controller('BinhLuanDanhSachCtrl', ['$scope', 'Custom', 'BinhLuan', binhLuanController]);
function binhLuanController($scope, Custom, BinhLuan) {
	var vm = this;

	var getAllBinhLuan = function(){
		BinhLuan.get()
			.success(function(result) {
				vm.binhluan = result.data;
			}).error(function(err){
		    });
	}

	vm.delete = function(ID){
		BinhLuan.delete(ID)
			.success(function(result) {
				Custom.setFlashMessages(result.data);
				vm.message = Custom.getFlashMessages();
				getAllBinhLuan();
			}).error(function(err){
		    });		
	}

	vm.message = Custom.getFlashMessages();

	getAllBinhLuan();

	Custom.setTitle('Bình Luận');
	Custom.setPath('binhluan/danhsach');
}