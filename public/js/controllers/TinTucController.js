'use strict';
var tinTucCtrl = angular.module('tinTucCtrl', []);

tinTucCtrl.controller('TinTucDanhSachCtrl', ['$scope', 'Custom', 'TinTuc', 'NhomTin', tinTucController]);
function tinTucController($scope, Custom, TinTuc, NhomTin) {
	var vm = this;

	$scope.currentPage = 1;
	$scope.pageSize = 7;

	var getAllTinTuc = function(){
		TinTuc.get()
			.success(function(result) {
				vm.tintuc = result.data;
			}).error(function(err){
		    });
	}

	vm.deleteTinTuc = function(ID){
		TinTuc.delete(ID)
			.success(function(result) {
				Custom.setFlashMessages(result.data);
				vm.message = Custom.getFlashMessages();
				getAllTinTuc();
			}).error(function(err){
		    });		
	}

	vm.message = Custom.getFlashMessages();

	getAllTinTuc();

	Custom.setTitle('Tin Tức');
	Custom.setPath('tintuc/danhsach');
}

tinTucCtrl.controller('TaoTinTucCtrl', ['$scope', '$location', '$window', 'Upload', '$timeout', 'Custom', 'TinTuc', 'NhomTin', taoTinTucController]);
function taoTinTucController($scope, $location, $window, Upload, $timeout, Custom, TinTuc, NhomTin) {
	var vm = this;

	vm.createPage = true;

	vm.taoTinTuc = function (file) {
		if (vm.file) { //check if from is valid
			vm.form.Anh = vm.file.name;
            vm.upload(vm.file); //call upload function
        }
        //--
		if(!vm.form.KichHoat)
			vm.form.KichHoat = "false";
		TinTuc.createData(vm.form)
			.success(function(result) {
				// console.log('result.data',result.data);
				Custom.setFlashMessages(result.data);
				$location.path('admin/tin-tuc');
			}).error(function(err){
		    });	

	}

	var getAllNhomTin = function(){
		NhomTin.get()
			.success(function(result) {
				vm.nhomtin = result.data
			}).error(function(err){
		    });
	}

    vm.upload = function (file,form) {
        Upload.upload({
            url: 'http://localhost:3000/api/tin-tuc', //webAPI exposed to upload the file
            data:{file:file} //pass file as data, should be user ng-model
            // data:{file:file,form:form} //pass file as data, should be user ng-model
        }).then(function (resp) { //upload function returns a promise
            if(resp.data.error_code === 0){ //validate success
                // $window.alert('Success ' + resp.config.data.file.name + 'uploaded. Response: ');
            } else {
                // $window.alert('an error occured');
            }
        }, function (resp) { //catch error
            console.log('Error status: ' + resp.status);
            // $window.alert('Error status: ' + resp.status);
        }, function (evt) { 
            console.log(evt);
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            // console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
            vm.progress = 'progress: ' + progressPercentage + '% '; // capture upload progress
        });
    };

	vm.message = Custom.getFlashMessages();
	getAllNhomTin();

	Custom.setTitle('Tạo Tin Tức');
	Custom.setPath('tintuc/formTinTuc');
}

tinTucCtrl.controller('CapNhatTinTucCtrl', ['$scope', '$routeParams', '$location', 'Upload', 'Custom', 'TinTuc', 'NhomTin', capNhatTinTucController]);
function capNhatTinTucController($scope, $routeParams, $location, Upload, Custom, TinTuc, NhomTin) {
	var vm = this;
	vm.updatePage = true;

	var getTinTuc = function(){
		TinTuc.getOne($routeParams.id)
			.success(function(result) {
				vm.form = result.data;
				console.log('result.data',result.data);
			}).error(function(err){
		    });
	}

	vm.update = function(file){
		console.log('data edit', vm.form);

		if (vm.file) { //check if from is valid
			vm.form.Anh = vm.file.name;
            vm.upload(vm.file); //call upload function
		}
		TinTuc.update(vm.form)
			.success(function(result) {
				Custom.setFlashMessages(result.data);
				$location.path('admin/tin-tuc');
			}).error(function(err){
		    });	        
	}

    vm.upload = function (file,form) {
        Upload.upload({
            url: 'http://localhost:3000/api/tin-tuc', //webAPI exposed to upload the file
            data:{file:file,form:form} //pass file as data, should be user ng-model
        }).then(function (resp) { //upload function returns a promise
            if(resp.data.error_code === 0){ //validate success
                // $window.alert('Success ' + resp.config.data.file.name + 'uploaded. Response: ');
            } else {
                // $window.alert('an error occured');
            }
        }, function (resp) { //catch error
            console.log('Error status: ' + resp.status);
            // $window.alert('Error status: ' + resp.status);
        }, function (evt) { 
            console.log(evt);
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            // console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
            vm.progress = 'progress: ' + progressPercentage + '% '; // capture upload progress
        });
    };


	var getAllNhomTin = function(){
		NhomTin.get()
			.success(function(result) {
				vm.nhomtin = result.data
			}).error(function(err){
		    });
	}

	getTinTuc();
	getAllNhomTin();

	Custom.setTitle('Cập Nhật Tin Tức');
	Custom.setPath('tintuc/formTinTuc');
}