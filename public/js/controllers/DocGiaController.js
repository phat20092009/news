'use strict';
var docGiaCtrl = angular.module('docGiaCtrl', []);

docGiaCtrl.controller('DocGiaDanhSachCtrl', ['$scope', 'Custom', 'DocGia', docgiaController]);
function docgiaController($scope, Custom, DocGia) {
	var vm = this;

	var getAllDocGia = function(){
		DocGia.get()
			.success(function(result) {
				vm.docgia = result.data;
			}).error(function(err){
		    });
	}
	getAllDocGia();

	Custom.setTitle('Đọc Giả');
	Custom.setPath('docgia/danhsach');
}