'use strict';
var authCtrl =  angular.module('authCtrl',[]);

// authCtrl.run(['$rootScope', '$location', 'User', function($rootScope, $location, User) {
// 	$rootScope.isLogin = false;
// 	$rootScope.$on('$routeChangeStart', function() {
// 		User.isLogin()
// 			.success(function(result) {
// 				$rootScope.isLogin = result.data;
// 				if($rootScope.isLogin && $location.path() == "/admin/login"){// nếu đã login rồi
// 					$location.path('admin/user');
// 				}
// 				if(!$rootScope.isLogin){// nếu chưa login
// 					$location.path('admin/login');
// 				}
// 			}).error(function(err){
// 		    });

// 	});
// }]);

authCtrl.controller('AuthLoginCtrl',['$scope', '$rootScope', '$location', '$localStorage', 'Custom', 'User',authController]);
function authController($scope, $rootScope, $location, $localStorage, Custom, User){
	var vm = this;
	var loginAPI = function(userData){
		User.login(userData)
			.success(function(result) {
				// vm.$storage = result.data;
				$rootScope.$storage = $localStorage.$default({
					user: result.data
				});
					// $location.path('admin/user');
				// console.log('result.data - admin',result.data);
				if(result.data){
					$location.path('admin/user');
				}else{
					vm.error = true;
				}
			}).error(function(err){
		    });
	}

	vm.login = function(input){
		loginAPI(input);
	}

	// Thiết lập đường dẫn đến View
	Custom.setTitle('Đăng nhập');
	Custom.setPath('login');
}
