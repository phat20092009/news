'use strict';
var quangCaoCtrl = angular.module('quangCaoCtrl', []);

quangCaoCtrl.controller('QuangCaoDanhSachCtrl', ['$scope', 'Custom', 'QuangCao', quangCaoController]);
function quangCaoController($scope, Custom, QuangCao) {
	var vm = this;

	var getAllQuangCao = function(){
		QuangCao.get()
			.success(function(result) {
				vm.quangcao = result.data;
			}).error(function(err){
		    });
	}

	vm.delete = function(ID){
		QuangCao.delete(ID)
			.success(function(result) {
				Custom.setFlashMessages(result.data);
				vm.message = Custom.getFlashMessages();
				getAllQuangCao();
			}).error(function(err){
		    });		
	}

	vm.message = Custom.getFlashMessages();

	getAllQuangCao();

	Custom.setTitle('Quảng Cáo');
	Custom.setPath('quangcao/danhsach');
}

quangCaoCtrl.controller('QuangCaoTaoCtrl', ['$scope', '$location', 'Custom', 'QuangCao', quangCaoTaoController]);
function quangCaoTaoController($scope, $location, Custom, QuangCao) {
	var vm = this;
	vm.createPage = true;

	vm.create = function(){
		var input = { "data":vm.form };
		console.log('input',input);
		QuangCao.create(input)
			.success(function(result) {
				vm.quangcao = result.data;
				Custom.setFlashMessages(result.alert);
				$location.path('admin/quang-cao');
			}).error(function(err){
		    });		
	}

	Custom.setTitle('Thêm Quảng Cáo');
	Custom.setPath('quangcao/form');
}

quangCaoCtrl.controller('QuangCaoSuaCtrl', ['$scope', '$routeParams', '$location', 'Custom', 'QuangCao', quangCaoSuaController]);
function quangCaoSuaController($scope, $routeParams, $location, Custom, QuangCao) {
	var vm = this;
	vm.updatePage = true;

	var getQuangCao = function(){
		QuangCao.getOne($routeParams.id)
			.success(function(result) {
				vm.form = result.data;
			}).error(function(err){
		    });
	}

	vm.update = function(){
		QuangCao.update(vm.form)
			.success(function(result) {
				Custom.setFlashMessages(result.data);
				$location.path('admin/quang-cao');
			}).error(function(err){
		    });	
	}

	getQuangCao();

	Custom.setTitle('Sửa Quảng Cáo');
	Custom.setPath('quangcao/form');
}