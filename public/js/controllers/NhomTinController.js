'use strict';
var nhomTinCtrl = angular.module('nhomTinCtrl', []);

nhomTinCtrl.controller('NhomTinDanhSachCtrl', ['$scope', 'Custom', 'NhomTin', nhomTinController]);
function nhomTinController($scope, Custom, NhomTin) {
	var vm = this;

	$scope.currentPage = 1;
	$scope.pageSize = 2;

	var getAllNhomTin = function(){
		NhomTin.get()
			.success(function(result) {
				vm.nhomtin = result.data;
			}).error(function(err){
		    });
	}

	vm.delete = function(ID){
		console.log('ID',ID);
		NhomTin.delete(ID)
			.success(function(result) {
				Custom.setFlashMessages(result.data);
				vm.message = Custom.getFlashMessages();
				getAllNhomTin();
			}).error(function(err){
		    });		
	}
	
	getAllNhomTin();
	vm.message = Custom.getFlashMessages();

	Custom.setTitle('Nhóm Tin');
	Custom.setPath('nhomtin/danhsach');
}

nhomTinCtrl.controller('NhomTinTaoCtrl', ['$scope', '$location', 'Custom', 'NhomTin', nhomTinTaoController]);
function nhomTinTaoController($scope, $location, Custom, NhomTin) {
	var vm = this;
	vm.createPage = true;

	vm.create = function(){
		var input = { "data":vm.form }
		NhomTin.create(input)
			.success(function(result) {
				vm.nhomtin = result.data;
				Custom.setFlashMessages(result.alert);
				$location.path('admin/nhom-tin');
				// console.log('tao nhom tin result.data',result.data);
			}).error(function(err){
		    });		
	}

	Custom.setTitle('Nhóm Tin');
	Custom.setPath('nhomtin/form');
}

nhomTinCtrl.controller('NhomTinSuaCtrl', ['$scope', '$routeParams', '$location','Custom', 'NhomTin', nhomTinSuaController]);
function nhomTinSuaController($scope, $routeParams, $location, Custom, NhomTin) {
	var vm = this;
	vm.updatePage = true;

	var getNhomTin = function(){
		NhomTin.getOne($routeParams.id)
			.success(function(result) {
				console.log('cap nhat', result.data)
				vm.form = result.data;
			}).error(function(err){
		    });
	}

	vm.update = function(){
		console.log('data edit', vm.form)
		if(vm.form.KichHoat == false)
			vm.form.KichHoat = "false";
		NhomTin.update(vm.form)
			.success(function(result) {
				Custom.setFlashMessages(result.data);
				$location.path('admin/nhom-tin');
			}).error(function(err){
		    });	
	}

	getNhomTin();

	Custom.setTitle('Nhóm Tin');
	Custom.setPath('nhomtin/form');
}