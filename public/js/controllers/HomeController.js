'use strict';
var homeCtrl = angular.module('homeCtrl', []);

homeCtrl.controller('HomeListCtrl', ['$scope', 'Custom', 'NhomTin', 'TinTuc', HomeController]);
function HomeController($scope, Custom, NhomTin, TinTuc) {
	var vm = this;

	var getAllNhomTin = function(){
		NhomTin.get()
			.success(function(result) {
				vm.nhomtin = result.data;
				console.log('vm.nhomtin',vm.nhomtin[0]._id);
				console.log('vm.nhomtin',vm.nhomtin[1]._id);
				console.log('vm.nhomtin',vm.nhomtin[2]._id);
				getAllTinTucByCat(vm.nhomtin[0]._id);
				getAllTinTucByCat2(vm.nhomtin[1]._id);
				getAllTinTucByCat3(vm.nhomtin[2]._id);
			}).error(function(err){
		    });
	}

	var getAllTinTucByCat = function(ID){
		TinTuc.getTinTucByCat(ID)
			.success(function(result) {
				vm.tintucByCat = result.data;
				vm.byCat = result.data[0].NhomTin.TenNhom;
				console.log('result.data',result.data);
			}).error(function(err){
		    });
	}

	var getAllTinTucByCat2 = function(ID){
		TinTuc.getTinTucByCat(ID)
			.success(function(result) {
				vm.tintucByCat2 = result.data;
				vm.byCat2 = result.data[0].NhomTin.TenNhom;
				console.log('result.data',result.data);
			}).error(function(err){
		    });
	}

	var getAllTinTucByCat3 = function(ID){
		TinTuc.getTinTucByCat(ID)
			.success(function(result) {
				vm.tintucByCat3 = result.data;
				vm.byCat3 = result.data[0].NhomTin.TenNhom;
				console.log('result.data',result.data);
			}).error(function(err){
		    });
	}

	var getAllTinTuc = function(){
		TinTuc.getSort('-NgayDang')
			.success(function(result) {
				vm.tintuc = result.data;
				// console.log(vm.tintuc.length)
			}).error(function(err){
		    });
	}

	vm.readmore = function(String){
		return String.substring(0, 54);
	}
	vm.readmoreNum = function(String,num){
		return String.substring(0, num);
	}
	getAllNhomTin();
	getAllTinTuc();

	Custom.setTitle('Trang Chủ');
	Custom.setPath('index');
};
homeCtrl.controller('TinTucCtrl', ['$scope', '$routeParams', 'Custom', 'NhomTin', 'TinTuc', 'BinhLuan', TinTucController]);
function TinTucController($scope, $routeParams, Custom, NhomTin, TinTuc, BinhLuan) {
	var vm = this;

	var getTinTuc = function(){
		TinTuc.getOne($routeParams.id)
			.success(function(result) {
				vm.chiTietTinTuc = result.data;
				// console.log('result.data[0]',result.data._id);
				getBinhLuan(result.data._id);
			}).error(function(err){
		    });
	}

	var getBinhLuan = function(ID){
		BinhLuan.getOneNew(ID)
			.success(function(result) {
				vm.binhluan = result.data;
				console.log('result.data binhluan',result.data);
			}).error(function(err){
		    });
	}

	var getAllTinTuc = function(){
		TinTuc.getSort('-NgayDang')
			.success(function(result) {
				vm.tintuc = result.data;
			}).error(function(err){
		    });
	}

	vm.readmoreNum = function(String,num){
		return String.substring(0, num);
	}

	var getAllNhomTin = function(){
		NhomTin.get()
			.success(function(result) {
				// vm.nhomtin = result.data;
				// console.log('vm.nhomtin',vm.nhomtin[0]._id);
				getAllTinTucByCat3(result.data[2]._id);
			}).error(function(err){
		    });
	}

	var getAllTinTucByCat3 = function(ID){
		TinTuc.getTinTucByCat(ID)
			.success(function(result) {
				vm.tintucByCat3 = result.data;
				vm.byCat3 = result.data[0].NhomTin.TenNhom;
				console.log('result.data',result.data);
			}).error(function(err){
		    });
	}

	vm.readmore = function(String){
		return String.substring(0, 57);
	}

	getTinTuc();
	getAllNhomTin();
	getAllTinTuc();

	Custom.setTitle('Tin Tức');
	Custom.setPath('tintuc');
};

homeCtrl.controller('TheLoaiCtrl', ['$scope', '$routeParams', 'Custom', 'NhomTin', 'TinTuc', TheLoaiController]);
function TheLoaiController($scope, $routeParams, Custom, NhomTin, TinTuc) {
	var vm = this;
	vm.theLoaiTin = false;

	var getTinTucbyTheLoai = function(){
		console.log('$routeParams.id',$routeParams.id);
		TinTuc.getCat($routeParams.id)
			.success(function(result) {
				vm.theLoaiTin = result.data;
				console.log('vm.theLoaiTin',vm.theLoaiTin);
			}).error(function(err){
				console.log('err',err);
		    });
	}


	vm.readmoreNum = function(String,num){
		return String.substring(0, num);
	}

	var getAllNhomTin = function(){
		NhomTin.get()
			.success(function(result) {
				// vm.nhomtin = result.data;
				console.log('result.data--', result.data);
				getAllTinTucByCat3(result.data[2]._id);
			}).error(function(err){
		    });
	}

	var getAllTinTucByCat3 = function(ID){
		TinTuc.getTinTucByCat(ID)
			.success(function(result) {
				// console.log('vm.tintucByCat3',result.data);
				vm.tintucByCat3 = result.data;
				vm.byCat3 = result.data[0].NhomTin.TenNhom;
				// console.log('result.data',result.data);
			}).error(function(err){
		    });
	}
	
	vm.readmore = function(String){
		return String.substring(0, 23);
	}

	// getTinTuc();

	getAllNhomTin();
	getTinTucbyTheLoai();

	Custom.setTitle('Thể Loại Tin');
	Custom.setPath('nhomtin');
}